﻿using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Core.Interfaces
{
    public interface INotaServicio
    {
        public Task<Nota> AgregarNota(Nota notaNueva);


        public Task<IList<NotaUsuario>> ListarNotas();
    }
}
