﻿using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Core.Interfaces
{
    public interface IRespuestaServicio
    {
        public Task<IList<RespuestaUsuario>> DevolverRespuestasPorComentario(int comentarioId);
        public Task<Respuesta> AgregarRespuesta(Respuesta respuesta);
    }
}
