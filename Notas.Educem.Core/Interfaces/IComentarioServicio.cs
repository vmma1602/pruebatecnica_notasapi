﻿using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Core.Interfaces
{
    public interface IComentarioServicio
    {
        public Task<IList<ComentarioUsuario>> DevolverComentariosPorNota(int notaId);

        public Task<Comentario> AgregarComentario(Comentario comentario);
    }
}
