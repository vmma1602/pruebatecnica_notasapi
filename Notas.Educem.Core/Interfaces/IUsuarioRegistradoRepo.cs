﻿using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Core.Interfaces
{
    public interface IUsuarioRegistradoRepo
    {
        public Task<UsuarioRegistrado> DevolverUsuarioRegistrado(int usuarioId);

        public Task<IList<UsuarioRegistrado>> DevolverUsuariosRegistrados();
    }
}
