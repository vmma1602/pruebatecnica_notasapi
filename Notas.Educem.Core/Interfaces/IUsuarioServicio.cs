﻿using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Core.Interfaces
{
    public interface IUsuarioServicio
    {
        public Task<UsuarioRegistrado> DevolverUsuarioRegistrado(int usuarioId);
        public Task<IList<UsuarioRegistrado>> DevolverUsuariosRegistrados();

    }
}
