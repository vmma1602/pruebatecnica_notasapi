﻿using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Core.Interfaces
{
    public interface INotaRepo
    {

        public Task<Nota> AgregarNota(Nota notaNueva);


        public Task<IList<NotaUsuario>> ListarNotas();

    }
}
