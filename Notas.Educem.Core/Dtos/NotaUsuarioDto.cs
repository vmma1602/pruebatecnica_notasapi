﻿namespace Notas.Educem.Core.Dtos
{
    public class NotaUsuarioDto
    {
        public int IdNota { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public byte[]? Fotografia { get; set; }
        public int FkUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public int IdRegistro { get; set; }
    }
}
