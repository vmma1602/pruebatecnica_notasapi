﻿namespace Notas.Educem.Core.Dtos
{
    public class NotaDto
    {
        public int IdNota { get; set; }
        public string Titulo { get; set; } = null!;
        public string? Descripcion { get; set; }
        public byte[]? Fotografia { get; set; }
        public int FkUsuario { get; set; }
    }
}
