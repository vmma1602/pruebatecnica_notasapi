﻿namespace Notas.Educem.Core.Dtos
{
    public class ComentarioDto
    {
        public int IdComentario { get; set; }
        public int FkUsuario { get; set; }
        public int FkNota { get; set; }
        public string Texto { get; set; }
        public DateTime FechaComentario { get; set; }
    }
}
