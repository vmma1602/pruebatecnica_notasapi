﻿namespace Notas.Educem.Core.Dtos
{
    public class ComentarioUsuarioDto
    {
        public string NombreUsuario { get; set; }
        public int IdRegistro { get; set; }
        public int IdComentario { get; set; }
        public int FkUsuario { get; set; }
        public int FkNota { get; set; }
        public string Texto { get; set; }
        public string TipoUsuario { get; set; }
        public DateTime FechaComentario { get; set; }
    }
}
