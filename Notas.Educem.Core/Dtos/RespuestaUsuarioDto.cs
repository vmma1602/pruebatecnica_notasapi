﻿namespace Notas.Educem.Core.Dtos
{
    public class RespuestaUsuarioDto
    {
        public int IdRespuesta { get; set; }
        public int FkUsuario { get; set; }
        public int FkComentario { get; set; }
        public string Texto { get; set; }
        public DateTime FechaRespuesta { get; set; }
        public string NombreUsuario { get; set; }
        public int IdRegistro { get; set; }
        public string TipoUsuario { get; set; }
    }
}
