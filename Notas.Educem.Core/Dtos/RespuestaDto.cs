﻿namespace Notas.Educem.Core.Dtos
{
    public class RespuestaDto
    {
        public int IdRespuesta { get; set; }
        public int FkUsuario { get; set; }
        public int FkComentario { get; set; }

        public string Texto { get; set; }
        public DateTime FechaRespuesta { get; set; }
    }
}
