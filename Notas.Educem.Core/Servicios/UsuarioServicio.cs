﻿using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Core.Servicios
{
    public class UsuarioServicio : IUsuarioServicio
    {
        private readonly IUsuarioRegistradoRepo _usuarioRegistradoRepo;
        public UsuarioServicio(IUsuarioRegistradoRepo usuarioRegistradoRepo)
        {
            _usuarioRegistradoRepo = usuarioRegistradoRepo;
        }
        public async Task<UsuarioRegistrado> DevolverUsuarioRegistrado(int usuarioId)
        {
            return await _usuarioRegistradoRepo.DevolverUsuarioRegistrado(usuarioId);
        }

        public async Task<IList<UsuarioRegistrado>> DevolverUsuariosRegistrados()
        {
            return await _usuarioRegistradoRepo.DevolverUsuariosRegistrados();
        }
    }
}
