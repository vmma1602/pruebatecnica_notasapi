﻿using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Core.Servicios
{
    public class ComentarioServicio : IComentarioServicio
    {
        private readonly IComentarioRepo _comentarioRepo;
        public ComentarioServicio(IComentarioRepo comentarioRepo)
        {
            _comentarioRepo = comentarioRepo;
        }
        public async Task<Comentario> AgregarComentario(Comentario comentario)
        {
            comentario.FechaComentario = DateTime.Now;
            return await _comentarioRepo.AgregarComentario(comentario);
        }

        public async Task<IList<ComentarioUsuario>> DevolverComentariosPorNota(int notaId)
        {
            return await _comentarioRepo.DevolverComentariosPorNota(notaId);
        }
    }
}
