﻿using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Core.Servicios
{
    public class NotaServicio : INotaServicio
    {
        private readonly INotaRepo _notaRepo;
        public NotaServicio(INotaRepo notaRepo)
        {
            _notaRepo = notaRepo;
        }

        public async Task<Nota> AgregarNota(Nota notaNueva)
        {
            return await _notaRepo.AgregarNota(notaNueva);
        }

        public async Task<IList<NotaUsuario>> ListarNotas()
        {
            return await _notaRepo.ListarNotas();
        }
    }
}
