﻿using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Core.Servicios
{
    public class RespuestaServicio : IRespuestaServicio
    {
        private readonly IRespuestaRepo _respuestaRepo;
        public RespuestaServicio(IRespuestaRepo respuestaRepo)
        {
            _respuestaRepo = respuestaRepo;
        }
        public async Task<Respuesta> AgregarRespuesta(Respuesta respuesta)
        {
            respuesta.FechaRespuesta = DateTime.Now;
            return await _respuestaRepo.AgregarRespuesta(respuesta);
        }

        public async Task<IList<RespuestaUsuario>> DevolverRespuestasPorComentario(int comentarioId)
        {
            return await _respuestaRepo.DevolverRespuestasPorComentario(comentarioId);
        }

    }
}
