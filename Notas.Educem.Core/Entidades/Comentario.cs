﻿namespace Notas.Educem.Core.Entidades
{
    public partial class Comentario
    {
        public Comentario()
        {
            Respuesta = new HashSet<Respuesta>();
        }

        public int IdComentario { get; set; }
        public int FkUsuario { get; set; }
        public int FkNota { get; set; }
        public string Texto { get; set; }
        public DateTime FechaComentario { get; set; }

        public virtual Nota FkNotaNavigation { get; set; } = null!;
        public virtual RegistroUsuario FkUsuarioNavigation { get; set; } = null!;
        public virtual ICollection<Respuesta> Respuesta { get; set; }
    }
}
