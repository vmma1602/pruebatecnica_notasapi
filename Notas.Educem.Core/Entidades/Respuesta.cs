﻿namespace Notas.Educem.Core.Entidades
{
    public partial class Respuesta
    {
        public int IdRespuesta { get; set; }
        public int FkUsuario { get; set; }
        public int FkComentario { get; set; }

        public string Texto { get; set; }
        public DateTime FechaRespuesta { get; set; }

        public virtual Comentario FkComentarioNavigation { get; set; } = null!;
        public virtual RegistroUsuario FkUsuarioNavigation { get; set; } = null!;
    }
}
