﻿namespace Notas.Educem.Core.Entidades
{
    public class UsuarioRegistrado
    {
        public int IdUsuario { get; set; }
        public int IdRegistro { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string TipoUsuario { get; set; }
    }
}
