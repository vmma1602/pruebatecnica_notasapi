﻿namespace Notas.Educem.Core.Entidades
{
    public class RespuestaUsuario : Respuesta
    {
        public string NombreUsuario { get; set; }
        public int IdRegistro { get; set; }
        public string TipoUsuario { get; set; }
    }
}
