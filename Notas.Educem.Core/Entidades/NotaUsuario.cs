﻿namespace Notas.Educem.Core.Entidades
{
    public class NotaUsuario : Nota
    {
        public string NombreUsuario { get; set; }
        public int IdRegistro { get; set; }
    }
}
