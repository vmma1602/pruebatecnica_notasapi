﻿namespace Notas.Educem.Core.Entidades
{
    public partial class Nota
    {
        public Nota()
        {
            Comentarios = new HashSet<Comentario>();
        }

        public int IdNota { get; set; }
        public string Titulo { get; set; } = null!;
        public string? Descripcion { get; set; }
        public byte[]? Fotografia { get; set; }
        public int FkUsuario { get; set; }

        public virtual RegistroUsuario FkUsuarioNavigation { get; set; } = null!;
        public virtual ICollection<Comentario> Comentarios { get; set; }
    }
}
