﻿namespace Notas.Educem.Core.Entidades
{
    public partial class RegistroUsuario
    {
        public RegistroUsuario()
        {
            Comentarios = new HashSet<Comentario>();
            Nota = new HashSet<Nota>();
            Respuesta = new HashSet<Respuesta>();
        }

        public int IdRegistro { get; set; }
        public int FkUsuario { get; set; }
        public string TipoUsuario { get; set; } = null!;

        public virtual Personal FkUsuarioNavigation { get; set; } = null!;
        public virtual ICollection<Comentario> Comentarios { get; set; }
        public virtual ICollection<Nota> Nota { get; set; }
        public virtual ICollection<Respuesta> Respuesta { get; set; }
    }
}
