﻿namespace Notas.Educem.Core.Entidades
{
    public partial class Personal
    {
        public int IdPersonal { get; set; }
        public string Apepaterno { get; set; } = null!;
        public string Apematerno { get; set; } = null!;
        public string Direccion { get; set; } = null!;
        public DateTime FechaIngreso { get; set; }
        public string Nombre { get; set; } = null!;

        public virtual RegistroUsuario? RegistroUsuario { get; set; }
    }
}
