﻿using Microsoft.EntityFrameworkCore;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;
using Notas.Educem.Infraestructura.Datos;

namespace Notas.Educem.Infraestructura.Repos
{
    public class RespuestaRepo : IRespuestaRepo
    {
        private readonly NotasDbContext _context;

        public RespuestaRepo(NotasDbContext context)
        {
            _context = context;
        }

        public async Task<Respuesta> AgregarRespuesta(Respuesta respuesta)
        {
            await _context.Respuestas.AddAsync(respuesta);
            await _context.SaveChangesAsync();
            int ultimoId = _context.Respuestas.Max(res => res.IdRespuesta);
            return await _context.Respuestas.Where(res => res.IdRespuesta == ultimoId).FirstOrDefaultAsync();
        }

        public async Task<IList<RespuestaUsuario>> DevolverRespuestasPorComentario(int comentarioId)
        {
            var respuestas = await (from respuesta in _context.Respuestas
                                    join registro in _context.RegistroUsuarios on respuesta.FkUsuario equals registro.IdRegistro
                                    join usuario in _context.Personal on registro.FkUsuario equals usuario.IdPersonal
                                    join comentario in _context.Comentarios on respuesta.FkComentario equals comentario.IdComentario
                                    where comentario.IdComentario == comentarioId
                                    select new RespuestaUsuario()
                                    {
                                        FechaRespuesta = respuesta.FechaRespuesta,
                                        NombreUsuario = $"{usuario.Nombre} {usuario.Apepaterno} {usuario.Apematerno}",
                                        IdRespuesta = respuesta.IdRespuesta,
                                        IdRegistro = registro.IdRegistro,
                                        Texto = respuesta.Texto,
                                        TipoUsuario = registro.TipoUsuario
                                    })
                                .OrderByDescending(resp => resp.FechaRespuesta)
                                .ToListAsync();

            return respuestas;
        }
    }
}
