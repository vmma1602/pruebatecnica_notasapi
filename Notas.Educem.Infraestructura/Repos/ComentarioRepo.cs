﻿using Microsoft.EntityFrameworkCore;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;
using Notas.Educem.Infraestructura.Datos;

namespace Notas.Educem.Infraestructura.Repos
{
    public class ComentarioRepo : IComentarioRepo
    {
        private readonly NotasDbContext _context;

        public ComentarioRepo(NotasDbContext context)
        {
            _context = context;
        }
        public async Task<Comentario> AgregarComentario(Comentario comentario)
        {
            await _context.Comentarios.AddAsync(comentario);
            await _context.SaveChangesAsync();
            int ultimoId = _context.Comentarios.Max(coment => coment.IdComentario);
            return await _context.Comentarios.Where(coment => coment.IdComentario == ultimoId).FirstOrDefaultAsync();
        }

        public async Task<IList<ComentarioUsuario>> DevolverComentariosPorNota(int notaId)
        {
            var query = await (from comentario in _context.Comentarios
                               join registro in _context.RegistroUsuarios on comentario.FkUsuario equals registro.IdRegistro
                               join usuario in _context.Personal on registro.FkUsuario equals usuario.IdPersonal
                               join nota in _context.Notas on comentario.FkNota equals nota.IdNota
                               where nota.IdNota == notaId
                               select new ComentarioUsuario()
                               {
                                   FechaComentario = comentario.FechaComentario,
                                   NombreUsuario = $"{usuario.Nombre} {usuario.Apepaterno} {usuario.Apematerno}",
                                   IdComentario = comentario.IdComentario,
                                   IdRegistro = registro.IdRegistro,
                                   Texto = comentario.Texto,
                                   TipoUsuario = registro.TipoUsuario
                               })
                               .OrderByDescending(c => c.FechaComentario)
                               .ToListAsync();

            return query;
        }
    }
}
