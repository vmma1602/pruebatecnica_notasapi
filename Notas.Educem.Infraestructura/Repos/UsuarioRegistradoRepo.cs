﻿using Microsoft.EntityFrameworkCore;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;
using Notas.Educem.Infraestructura.Datos;

namespace Notas.Educem.Infraestructura.Repos
{
    public class UsuarioRegistradoRepo : IUsuarioRegistradoRepo
    {
        private readonly NotasDbContext _context;

        public UsuarioRegistradoRepo(NotasDbContext context)
        {
            _context = context;
        }

        public async Task<UsuarioRegistrado> DevolverUsuarioRegistrado(int usuarioId)
        {
            return await (from usuario in _context.Personal
                          join registro in _context.RegistroUsuarios on usuario.IdPersonal equals registro.FkUsuario
                          select new UsuarioRegistrado()
                          {
                              IdUsuario = usuario.IdPersonal,
                              IdRegistro = registro.IdRegistro,
                              Nombre = usuario.Nombre,
                              ApellidoPaterno = usuario.Apepaterno,
                              ApellidoMaterno = usuario.Apematerno,
                              TipoUsuario = registro.TipoUsuario
                          })
                         .Where(usuario => usuario.IdUsuario == usuarioId)
                         .FirstOrDefaultAsync();
        }

        public async Task<IList<UsuarioRegistrado>> DevolverUsuariosRegistrados()
        {
            return await _context.Personal
                 .Join(_context.RegistroUsuarios,
                     usr => usr.IdPersonal,
                     registro => registro.FkUsuario,
                    (usr, registro) => new UsuarioRegistrado()
                    {
                        IdUsuario = usr.IdPersonal,
                        IdRegistro = registro.IdRegistro,
                        Nombre = usr.Nombre,
                        ApellidoPaterno = usr.Apepaterno,
                        ApellidoMaterno = usr.Apematerno,
                        TipoUsuario = registro.TipoUsuario
                    })
                 .ToListAsync();
        }
    }
}
