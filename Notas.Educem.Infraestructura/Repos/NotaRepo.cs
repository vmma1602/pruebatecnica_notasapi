﻿using Microsoft.EntityFrameworkCore;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;
using Notas.Educem.Infraestructura.Datos;


namespace Notas.Educem.Infraestructura.Repos
{
    public class NotaRepo : INotaRepo
    {
        private readonly NotasDbContext _context;

        public NotaRepo(NotasDbContext context)
        {
            _context = context;
        }

        public async Task<Nota> AgregarNota(Nota notaNueva)
        {
            await _context.Notas.AddAsync(notaNueva);
            await _context.SaveChangesAsync();
            int ultimoId = _context.Notas.Max(nota => nota.IdNota);
            return await _context.Notas.Where(nota => nota.IdNota == ultimoId).FirstOrDefaultAsync();

        }

        public async Task<IList<NotaUsuario>> ListarNotas()
        {
            var query = await (from nota in _context.Notas
                               join registro in _context.RegistroUsuarios on nota.FkUsuario equals registro.IdRegistro
                               join usuario in _context.Personal on registro.FkUsuario equals usuario.IdPersonal
                               select new NotaUsuario()
                               {
                                   Titulo = nota.Titulo,
                                   Descripcion = nota.Descripcion,
                                   Fotografia = nota.Fotografia,
                                   NombreUsuario = $"{usuario.Nombre} {usuario.Apepaterno} {usuario.Apematerno}",
                                   IdNota = nota.IdNota,
                                   IdRegistro = registro.IdRegistro
                               }).ToListAsync();

            return query;
        }
    }
}
