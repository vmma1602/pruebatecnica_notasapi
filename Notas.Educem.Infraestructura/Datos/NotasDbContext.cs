﻿using Microsoft.EntityFrameworkCore;
using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Infraestructura.Datos
{
    public partial class NotasDbContext : DbContext
    {
        public NotasDbContext()
        {
        }

        public NotasDbContext(DbContextOptions<NotasDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Comentario> Comentarios { get; set; } = null!;
        public virtual DbSet<Nota> Notas { get; set; } = null!;
        public virtual DbSet<Personal> Personal { get; set; } = null!;
        public virtual DbSet<RegistroUsuario> RegistroUsuarios { get; set; } = null!;
        public virtual DbSet<Respuesta> Respuestas { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<Comentario>(entity =>
            {
                entity.HasKey(e => e.IdComentario)
                    .HasName("PRIMARY");

                entity.ToTable("comentarios");

                entity.HasIndex(e => e.FkNota, "fkNota");

                entity.HasIndex(e => e.FkUsuario, "fkUsuario");

                entity.Property(e => e.IdComentario).HasColumnName("idComentario");

                entity.Property(e => e.FechaComentario)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaComentario");

                entity.Property(e => e.FkNota).HasColumnName("fkNota");

                entity.Property(e => e.Texto).HasColumnName("texto");

                entity.Property(e => e.FkUsuario).HasColumnName("fkUsuario");

                entity.HasOne(d => d.FkNotaNavigation)
                    .WithMany(p => p.Comentarios)
                    .HasForeignKey(d => d.FkNota)
                    .HasConstraintName("comentarios_ibfk_2");

                entity.HasOne(d => d.FkUsuarioNavigation)
                    .WithMany(p => p.Comentarios)
                    .HasForeignKey(d => d.FkUsuario)
                    .HasConstraintName("comentarios_ibfk_1");
            });



            modelBuilder.Entity<Nota>(entity =>
            {
                entity.HasKey(e => e.IdNota)
                    .HasName("PRIMARY");

                entity.ToTable("notas");

                entity.HasIndex(e => e.FkUsuario, "fkUsuario");

                entity.Property(e => e.IdNota).HasColumnName("idNota");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.FkUsuario).HasColumnName("fkUsuario");

                entity.Property(e => e.Fotografia)
                    .HasColumnType("blob")
                    .HasColumnName("fotografia");

                entity.Property(e => e.Titulo)
                    .HasMaxLength(255)
                    .HasColumnName("titulo");

                entity.HasOne(d => d.FkUsuarioNavigation)
                    .WithMany(p => p.Nota)
                    .HasForeignKey(d => d.FkUsuario)
                    .HasConstraintName("notas_ibfk_1");
            });



            modelBuilder.Entity<Personal>(entity =>
            {
                entity.HasKey(e => e.IdPersonal)
                    .HasName("PRIMARY");

                entity.ToTable("personal");

                entity.Property(e => e.IdPersonal).HasColumnName("idPersonal");

                entity.Property(e => e.Apematerno)
                    .HasMaxLength(100)
                    .HasColumnName("apematerno");

                entity.Property(e => e.Apepaterno)
                    .HasMaxLength(100)
                    .HasColumnName("apepaterno");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(100)
                    .HasColumnName("direccion");

                entity.Property(e => e.FechaIngreso)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaIngreso");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .HasColumnName("nombre");
            });




            modelBuilder.Entity<RegistroUsuario>(entity =>
            {
                entity.HasKey(e => e.IdRegistro)
                    .HasName("PRIMARY");

                entity.ToTable("registro_usuarios");

                entity.HasIndex(e => e.FkUsuario, "fkUsuario")
                    .IsUnique();

                entity.Property(e => e.IdRegistro).HasColumnName("idRegistro");

                entity.Property(e => e.FkUsuario).HasColumnName("fkUsuario");

                entity.Property(e => e.TipoUsuario)
                    .HasMaxLength(100)
                    .HasColumnName("tipoUsuario");

                entity.HasOne(d => d.FkUsuarioNavigation)
                    .WithOne(p => p.RegistroUsuario)
                    .HasForeignKey<RegistroUsuario>(d => d.FkUsuario)
                    .HasConstraintName("registro_usuarios_ibfk_1");
            });




            modelBuilder.Entity<Respuesta>(entity =>
            {
                entity.HasKey(e => e.IdRespuesta)
                    .HasName("PRIMARY");

                entity.ToTable("respuestas");

                entity.HasIndex(e => e.FkComentario, "fkComentario");

                entity.HasIndex(e => e.FkUsuario, "fkUsuario");

                entity.Property(e => e.IdRespuesta).HasColumnName("idRespuesta");

                entity.Property(e => e.Texto).HasColumnName("texto");

                entity.Property(e => e.FechaRespuesta)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaRespuesta");

                entity.Property(e => e.FkComentario).HasColumnName("fkComentario");

                entity.Property(e => e.FkUsuario).HasColumnName("fkUsuario");

                entity.HasOne(d => d.FkComentarioNavigation)
                    .WithMany(p => p.Respuesta)
                    .HasForeignKey(d => d.FkComentario)
                    .HasConstraintName("respuestas_ibfk_2");

                entity.HasOne(d => d.FkUsuarioNavigation)
                    .WithMany(p => p.Respuesta)
                    .HasForeignKey(d => d.FkUsuario)
                    .HasConstraintName("respuestas_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
