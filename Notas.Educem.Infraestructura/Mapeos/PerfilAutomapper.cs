﻿using AutoMapper;
using Notas.Educem.Core.Dtos;
using Notas.Educem.Core.Entidades;

namespace Notas.Educem.Infraestructura.Mapeos
{
    public class PerfilAutomapper : Profile
    {
        public PerfilAutomapper()
        {
            CreateMap<Nota, NotaDto>().ReverseMap();
            CreateMap<NotaUsuario, NotaUsuarioDto>().ReverseMap();
            CreateMap<Comentario, ComentarioDto>().ReverseMap();
            CreateMap<ComentarioUsuario, ComentarioUsuarioDto>().ReverseMap();
            CreateMap<Respuesta, RespuestaDto>().ReverseMap();
            CreateMap<RespuestaUsuario, RespuestaUsuarioDto>().ReverseMap();
            CreateMap<UsuarioRegistrado, UsuarioRegistradoDto>().ReverseMap();
        }
    }
}
