using Microsoft.EntityFrameworkCore;
using Notas.Educem.Core.Interfaces;
using Notas.Educem.Core.Servicios;
using Notas.Educem.Infraestructura.Datos;
using Notas.Educem.Infraestructura.Repos;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddTransient<INotaRepo, NotaRepo>();
builder.Services.AddTransient<INotaServicio, NotaServicio>();
builder.Services.AddTransient<IUsuarioRegistradoRepo, UsuarioRegistradoRepo>();
builder.Services.AddTransient<IUsuarioServicio, UsuarioServicio>();
builder.Services.AddTransient<IComentarioRepo, ComentarioRepo>();
builder.Services.AddTransient<IComentarioServicio, ComentarioServicio>();
builder.Services.AddTransient<IRespuestaRepo, RespuestaRepo>();
builder.Services.AddTransient<IRespuestaServicio, RespuestaServicio>();


builder.Services.AddDbContext<NotasDbContext>(options =>
    {
        options.UseMySql(builder.Configuration.GetConnectionString("notasEducem"), ServerVersion.Parse("8.0.28"));
    });



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseCors(opt => opt.AllowAnyHeader()
                      .AllowAnyMethod()
                      .AllowAnyOrigin());

app.UseAuthorization();

app.MapControllers();

app.Run();
