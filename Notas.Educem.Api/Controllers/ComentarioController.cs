﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Notas.Educem.Core.Dtos;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComentarioController : ControllerBase
    {
        private readonly IComentarioServicio _comentarioServicio;
        private readonly IMapper _mapper;
        public ComentarioController(IComentarioServicio comentarioServicio, IMapper mapper)
        {
            _comentarioServicio = comentarioServicio;
            _mapper = mapper;
        }

        [HttpGet("ListarComentariosPorNota")]
        public async Task<IActionResult> ListarComentariosPorNota([FromQuery] int notaId)
        {
            var comentarios = await _comentarioServicio.DevolverComentariosPorNota(notaId);
            var comentariosDto = _mapper.Map<IList<ComentarioUsuarioDto>>(comentarios);
            if (comentariosDto == null) return BadRequest();

            return Ok(comentariosDto);
        }

        [HttpPost("AgregarComentario")]
        public async Task<IActionResult> AgregarComentario([FromBody] ComentarioDto comentarioDto)
        {
            var comentarioNuevo = _mapper.Map<Comentario>(comentarioDto);
            var comentario = await _comentarioServicio.AgregarComentario(comentarioNuevo);
            if (comentario == null) return BadRequest();

            return Ok(comentario);
        }

    }
}
