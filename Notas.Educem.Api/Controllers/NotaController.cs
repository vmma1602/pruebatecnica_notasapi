﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Notas.Educem.Core.Dtos;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotaController : ControllerBase
    {

        private readonly INotaServicio _notaServicio;
        private readonly IMapper _mapper;

        public NotaController(INotaServicio notaServicio, IMapper mapper)
        {
            _notaServicio = notaServicio;
            _mapper = mapper;
        }


        [HttpGet("ListarNotas")]
        public async Task<IActionResult> ListarNotas()
        {
            var notas = await _notaServicio.ListarNotas();
            var notasDto = _mapper.Map<IList<NotaUsuarioDto>>(notas);
            if (notasDto == null) return BadRequest();

            return Ok(notasDto);
        }

        [HttpPost("AgregarNota")]
        public async Task<IActionResult> AgregarNota([FromBody] NotaDto notaDto)
        {
            var notaNueva = _mapper.Map<Nota>(notaDto);
            var nota = await _notaServicio.AgregarNota(notaNueva);
            if (nota == null) return BadRequest();

            return Ok(nota);
        }
    }
}
