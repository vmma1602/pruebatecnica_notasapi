﻿using Microsoft.AspNetCore.Mvc;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioServicio _usuarioServicio;
        public UsuarioController(IUsuarioServicio usuarioServicio)
        {
            _usuarioServicio = usuarioServicio;
        }


        [HttpGet("DevolverUsuarioPorId")]
        public async Task<IActionResult> DevolverUsuarioPorId([FromQuery] int usuarioId)
        {
            var usuario = await _usuarioServicio.DevolverUsuarioRegistrado(usuarioId);
            if (usuario == null) return BadRequest();

            return Ok(usuario);
        }


        [HttpGet("DevolverUsuariosRegistrados")]
        public async Task<IActionResult> DevolverUsuariosRegistrados()
        {
            var usuarios = await _usuarioServicio.DevolverUsuariosRegistrados();
            if (usuarios == null) return BadRequest();

            return Ok(usuarios);
        }
    }
}
