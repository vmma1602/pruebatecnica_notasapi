﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Notas.Educem.Core.Dtos;
using Notas.Educem.Core.Entidades;
using Notas.Educem.Core.Interfaces;

namespace Notas.Educem.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RespuestaController : ControllerBase
    {
        private readonly IRespuestaServicio _respuestaServicio;
        private readonly IMapper _mapper;
        public RespuestaController(IRespuestaServicio respuestaServicio, IMapper mapper)
        {
            _respuestaServicio = respuestaServicio;
            _mapper = mapper;
        }

        [HttpGet("ListarRespuestasPorComentario")]
        public async Task<IActionResult> ListarRespuestasPorComentario([FromQuery] int comentarioId)
        {
            var respuestas = await _respuestaServicio.DevolverRespuestasPorComentario(comentarioId);
            var respuestasDto = _mapper.Map<IList<RespuestaUsuarioDto>>(respuestas);
            if (respuestasDto == null) return BadRequest();

            return Ok(respuestasDto);
        }

        [HttpPost("AgregarRespuesta")]
        public async Task<IActionResult> AgregarRespuesta([FromBody] RespuestaDto respuestaDto)
        {
            var respuestaNueva = _mapper.Map<Respuesta>(respuestaDto);
            var resp = await _respuestaServicio.AgregarRespuesta(respuestaNueva);
            if (resp == null) return BadRequest();

            return Ok(resp);
        }
    }
}
